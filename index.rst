.. |label| replace:: Artikel Black- und White-Listing
.. |snippet| replace:: FvArticleBlackWhiteListing
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.10
.. |Version| replace:: 2.2.10
.. |php| replace:: 7.4

|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Mit diesen Plugin können Sie entweder Artikel sichtbar/unsichtbar machen oder kaufbar und nichtkaufbar konfigurieren.
Es gibt immer eine Fallback-Blackliste wo alle Kunden (im Standard) zugeordnet sind.

Frontend
--------


Backend
-------
Hier werden alle Artikel aufgelistet und es kann in der Fallback-Blackliste jeder Artikel einzeln markiert oder demarkiert werden.
Es ist auch möglich alle pro Seite zu markieren oder zu demarkieren. Auch alle Artikel unabhängig von der Seite können Sie auf markieren oder demarkieren setzen.

.. image:: FvArticleBlackWhiteListing1.png

Je nachdem was in der Konfiguration eingestellt ist, werden die Artikel sichtbar/unsichtbar oder kaufbar/nicht kaufbar. (ggf. müssen die Texte in den Textbausteinen geändert werden)

.. image:: FvArticleBlackWhiteListing2.png

Wenn das Freitext-Feld fv_black_white_list unter s_user_attributes existiert, dann werden neue Whitelists in dem Plugin angezeigt und der Kunde hat diese Konfiguration zugewiesen. Somit können Sie Gruppen erstellen wo mehrere Kunden hinterlegt sind und diese in dem Plug-in entsprechend konfigurieren.


technische Beschreibung
------------------------
Wenn ein neuer Artikel hinzukommt, wird dieser immer negativ gesetzt. Entweder nicht sichtbar oder nicht kaufbar.

Modifizierte Template-Dateien
-----------------------------
Resources/views/frontend/detail/buy.tpl

kompatibel mit anderen Plugins
------------------------------

:User-Berechtigungen für Konzerne: FvUserPermissions

